(function (require) {

    var mongoose = require('mongoose');
    var Schema = mongoose['Schema'];

    var UserScheme = new Schema({
        name: {type: String},
        deviceId: {type: String}
    });

    module.exports = mongoose.model("User", UserScheme);

})(require);