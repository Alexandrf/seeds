(function (require) {

    var mongoose = require('mongoose');
    var Schema = mongoose['Schema'];

    var Question = require('./question');

    var PackageScheme = new Schema({
        code: {type: String},
        questions: [Question['schema']]
    });

    module.exports = mongoose.model("Package", PackageScheme);

})(require);