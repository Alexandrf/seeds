(function (require) {

    var mongoose = require('mongoose');
    var Schema = mongoose['Schema'];

    var QuestionScheme = new Schema({
        code: {type: String},
        shortName: {type: String},
        text: {type: String},
        tooltip1: {type: String},
        tooltip2: {type: String},
        answer: {type: String}
    });

    module.exports = mongoose.model("Question", QuestionScheme);

})(require);