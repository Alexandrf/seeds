(function (require) {

    var User = require('./models/user');
    var Question = require('./models/question');
    var Package = require('./models/package');

    module.exports = function () {
        return {
            createUser: function (data, handler) {

                var user = new User({
                    name: data.name,
                    deviceId: data.deviceId
                });

                user.save(function (error, model) {

                    if (error) {
                        handler.failure(error);
                    }

                    handler.success({
                        id: model['_id'].toString()
                    });
                });
            },
            findUserById: function (userId, handler) {
                user.findById(userId, function (error, model) {

                    if (error) {
                        handler.failure(error);
                    }

                    handler.success(model);
                });
            },
            findUserByDeviceId: function (deviceId, handler) {
                user.findOne({
                    deviceId: deviceId
                }, function (error, model) {

                    if (error) {
                        handler.failure(error);
                    }

                    handler.success(model);
                });
            },

            createQuestion:function (data, handler)
            {
                var question = new Question({
                    code: data.code,
                    shortName: data.shortName,
                    text: data.text,
                    tooltip1: data.tooltip1,
                    tooltip2: data.tooltip2,
                    answer: data.answer
                });

                question.save(function (error, model){
                    if(error)
                    {
                        handler.failure(error);
                    }

                    handler.success({
                        code: model['_id'].toString()
                    });
                });
            }
        }
    };

})(require);