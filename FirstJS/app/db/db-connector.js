(function (require) {

    var mongoose = require('mongoose');

    module.exports = {
        connect: function (handler) {

            mongoose.connect("mongodb://localhost:27017/db/local");

            var db = mongoose['connection'];

            db.on('error', function (error) {

                console.log('connection error: ' + error.message);

                handler.failure(error);
            });

            db.once('open', function () {

                console.log("Connected to DB!");

                var dbProvider = require('./db-provider')();
                handler.success(dbProvider);
            });
        }
    };

})(require);