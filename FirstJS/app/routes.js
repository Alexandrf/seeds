(function (require) {

    module.exports = function (app, dbProvider) {

        app.get('/', function (request, response) {
            response.render('index.ejs');
        });

        app.post('/api/sign-in/', function (request, response) {

            var name = request.body['name'];
            var deviceId = request.body['deviceId'];

            dbProvider.findUserByDeviceId(deviceId, {
                success: function (model) {
                    if (model) {
                        response.send({
                            status: true,
                            data: {
                                id: model['_id'].toString()
                            }
                        });
                    }else{
                        response.send({
                            status: false,
                            error: new Error('User not found')
                        });
                    }
                },
                failure: function (error) {
                    response.send({
                        status: false,
                        error: error
                    });
                }
            });
        });

        app.post('/api/sign-up/', function (request, response) {

            var name = request.body['name'];
            var deviceId = request.body['deviceId'];

            dbProvider.findUserByDeviceId(deviceId, {
                success: function (model) {
                    if (model) {
                        response.send({
                            status: false,
                            error: new Error('User already exists')
                        });
                    } else {
                        dbProvider.createUser({
                            name: name,
                            deviceId: deviceId
                        }, {
                            success: function (model) {
                                response.send({
                                    status: true,
                                    data: model
                                });
                            },
                            failure: function (error) {
                                response.send({
                                    status: false,
                                    error: error
                                });
                            }
                        });
                    }
                },
                failure: function (error) {
                    response.send({
                        status: false,
                        error: error
                    });
                }
            });
        });
    };

})(require);