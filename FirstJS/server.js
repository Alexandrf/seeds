(function (require) {

    var express = require('express');
    var app = express();
    var server = require('http').Server(app);
    var port = 8080;

    var dbConnector = require('./app/db/db-connector');
    dbConnector.connect({
        success: function (dbProvider) {

            app.configure(function () {

                app.use(express.logger('dev'));
                app.use(express.cookieParser());
                app.use(express.bodyParser());
                app.use(express.session({
                    secret: 'secret_key'
                }));

                app.set('view engine', 'ejs');

            });

            require('./app/routes')(app, dbProvider);

//            dbProvider.createQuestion({
//                code: 'code1',
//                shortName: 'shortName1',
//                text: 'text1',
//                tooltip1: 'tooltip1',
//                tooltip2: 'tooltip2',
//                answer: 'answer'
//            },{
//                success: function (model) {
////                    response.send({
////                        status: true,
////                        data: model
////                    });
//                },
//                failure: function (error) {
////                    response.send({
////                        status: false,
////                        error: error
////                    });
//                }
//            });

            server.listen(port);

            console.log('The magic happens on port ' + port);
        },
        failure: function (error) {
        }
    });

})(require);