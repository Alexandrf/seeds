﻿using Newtonsoft.Json;
using SeedsStore.DBContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebMatrix.WebData;

namespace SeedsStore.DAL
{
    public static class TabUser
    {
        public static string GetJSonUser(string userName)
        {
            using(var context = new SeedsStoreContext())
            {
                var userId = WebSecurity.GetUserId(userName);

                var user = context.UserProfiles.First(x => x.Id == userId);

                var response = JsonConvert.SerializeObject(user);

                return response;
            }
        }
    }
}