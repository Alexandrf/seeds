﻿
var basePath = "http://localhost:50989/";

var App = {};


function highlightField(field) {
    var obj = $(field);

    var delay = 130;

    obj.addClass("highlighted");

    setTimeout(function () {
        obj.removeClass("highlighted")
        setTimeout(function () {
            obj.addClass("highlighted")
            setTimeout(function () {
                obj.removeClass("highlighted")
                setTimeout(function () {
                    obj.addClass("highlighted")
                    setTimeout(function () {
                        obj.removeClass("highlighted")
                        setTimeout(function () {
                            obj.addClass("highlighted")
                            setTimeout(function () {
                                obj.removeClass("highlighted")

                            }, delay)
                        }, delay)
                    }, delay)
                }, delay)
            }, delay)
        }, delay)
    }, delay)
};




var User = Backbone.Model.extend({
    default: {
        'FirstName': "",
        'LastName': "",
        'Phone': "",
        'Email': ""
    },

    url: function () {
        return basePath + "registration"
    },

    validate: function (attrs, options) {
        var errors = [];

        if (attrs.FirstName == "") {
            errors.push({
                error: "First name is required",
                field: "FirstName"
            });
        }

        if (attrs.LastName == "") {
            errors.push({
                error: "Last name is required",
                field: "LastName"
            });
        }

        if (attrs.Phone.length > 0) {

            var regExNumber = /^[0-9]+$/;
            if (!regExNumber.test(attrs.Phone)) {

                errors.push({
                    error: "Phone number must contains only numbers",
                    field: "Phone"
                });

            } else if (attrs.Phone.length != 10) {

                errors.push({
                    error: "Phone number must contains 10 numbers",
                    field: "Phone"
                });
            }
        }


        if (options.withEmail) {
            if (attrs.Email == "") {
                errors.push({
                    error: "Email is required",
                    field: "Email"
                });
            }
            else {
                var regEx = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                if (!regEx.test(attrs.Email)) {
                    errors.push({
                        error: "Email has bad format",
                        field: "Email"
                    });
                }
            }
        }

        if (options.withCredentials) {
            if (attrs.Password == "") {
                errors.push({
                    error: "Password is required",
                    field: "Password"
                });
            };

            if (attrs.ConfPassword == "") {
                errors.push({
                    error: "Confirmation password is required",
                    field: "ConfPassword"
                });
            };

            if (attrs.Password != attrs.ConfPassword) {
                errors.push({
                    error: "Passwords are are different",
                    field: "ConfPassword"
                });
            };
        }

        if (errors.length > 0) {
            return errors;
        }
    }
});

App.User = new User;

App.User.refresh = function () {
    this.fetch({
        success: function (mod) {
            gEvents.trigger("adminRefreshed")
            //alert("success")
        },
        error: function (mod, err) {
            if (err.status == 403) {
                logout();
                return;
            }
            /*            if (err.status == 401) {
                            logout();
                            return;
                        }*/
        }
    })
}