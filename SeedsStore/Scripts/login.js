﻿$(document).ready(function () {

    //// UserEdit view
    var Login = Backbone.View.extend({
        model: new User,
        initialize: function () {

        },
        events: {
            'click [data-id="login"]': "onLogin"
        },
        onLogin: function () {

            var message = "";

            var loginObj = {
                "Email": this.$('[data-id="Email"]').val(),
                "Password": this.$('[data-id="Password"]').val(),
                "RememberMe": this.$('[data-id="RememberMe"]').is(":checked") ? "true" : "false"
            }

            if (loginObj.Email == "") {
                highlightField($('[data-id="Email"]'));

                message += "Please enter email";
                //Messenger().post({
                //    message: "Please enter your user name",
                //    type: 'error',
                //    showCloseButton: true
                //});
                //  return;
            }

            var regEx = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if (!regEx.test(loginObj.Email)) {
                highlightField($('[data-id="Email"]'));

                if (message == "") {
                    message += "Please enter correct email";
                }

                //Messenger().post({
                //    message: "Email is not valid",
                //    type: 'error',
                //    showCloseButton: true
                //});
                //return;
            }

            if (loginObj.Password == "") {
                highlightField($('[data-id="Password"]'));

                if (message == "") {
                    message += "Please enter password";
                } else {
                    message += " and password";
                }
                //Messenger().post({
                //    message: "Please enter password",
                //    type: 'error',
                //    showCloseButton: true
                //});
               
            }

            if (message != "") {
                vex.dialog.alert({
                    message: message,
                    className: 'vex-theme-default'
                });
                
                return;
            }

            $.ajax(basePath + 'login', {
                data: loginObj,
                dataType: "text",
                type: "POST"
            }).done(function () {
                window.location = basePath + "login"
            }).fail(function (err) {

                //if (err.status == 409) {
                //    window.location.href = "home"
                //}
                if (err.status == 401) {

                    vex.dialog.alert({
                        message: "Please check Password or Email",
                        className:'vex-theme-default'
                    });

                    //Messenger().post({
                    //    message: "Email or password are incorrect",
                    //    type: 'error',
                    //    showCloseButton: true
                    //});
                }
            })
        },
        render: function () {

            return this;
        }
    });


    var login = new Login({
        el: $('[data-id="login-form"]')
    });

});
