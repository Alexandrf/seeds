﻿var Registration = Backbone.View.extend({

    el:"[data-id=registration-form]",

    events: {
        'click [data-id="registration"]': "onRegistration",
        'change input[data-id]': "checkValidData"
    },

    checkValidData: function(e) {
        var value = e.currentTarget.value;
        var $el = this.$(e.currentTarget);
        if (value) {
            $el.parent().removeClass('has-error').find('.help-block').remove();
        } else {
            //var palceholder = $el.attr("placeholder");
            //$el.parent().addClass('has-error').append('<span class="help-block">' + palceholder + '</span>');
            this.model.isValid();
        }
    },

    initialize: function () {
        var self = this;

        this.model.on("invalid", function (model, error) {
                if (error && error.length > 0) {
                    for (var i = 0; i < error.length; i++) {
                        var err = error[i];
                        if (err.field) {
                            $(self.el).find('[data-id="' + err.field + '"]').parent().addClass('has-error');
                            $(self.el).find('[data-id="' + err.field + '"]').parent().find('.help-block').remove();
                            $(self.el).find('[data-id="' + err.field + '"]').parent().append('<span class="help-block">' + err.error + '</span>');

                            //highlightField($this.$('[data-id="' + err.field + '"]'));
                        }

                        //Messenger().post({
                        //    message: err.error,
                        //    type: 'error',
                        //    showCloseButton: true
                        //});
                    }
                }
            });
    },


    onRegistration: function () {

        this.model.set("FirstName", this.$('[data-id="FirstName"]').val())
        this.model.set("LastName", this.$('[data-id="LastName"]').val())
        this.model.set("Phone", this.$('[data-id="Phone"]').val())
        this.model.set("Email", this.$('[data-id="Email"]').val())
        this.model.set("Password", this.$('[data-id="Password"]').val())
        this.model.set("ConfPassword", this.$('[data-id="ConfPassword"]').val())
        //this.model.fetch();

        //$.ajax({
        //    url: "http://localhost:50989/api/values/Method",
        //    type: "post",
        //    contentType: "application/json",
        //    data: JSON.stringify(this.model.toJSON()),
          
        //    success: function (data) {
        //        alert(data)
        //    }
        //})

       

        this.model.save(null, {
            withEmail: true,
            withCredentials: true,
            success: function () {

                Messenger().post({
                    message: "Congratilation! Your account was successfully registered.",
                    type: 'info',
                    showCloseButton: true
                });
            }});
    }   
});

var registration = new Registration({
    model: App.User,
    el: $('[data-id="registration-form"]')
});