namespace SeedsStore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddColumnStreet : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserProfiles", "Street", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.UserProfiles", "Street");
        }
    }
}
