namespace SeedsStore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateColumnInUserProfile : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserProfiles", "PhoneNumber", c => c.String());
            DropColumn("dbo.UserProfiles", "Street");
        }
        
        public override void Down()
        {
            AddColumn("dbo.UserProfiles", "Street", c => c.String());
            DropColumn("dbo.UserProfiles", "PhoneNumber");
        }
    }
}
