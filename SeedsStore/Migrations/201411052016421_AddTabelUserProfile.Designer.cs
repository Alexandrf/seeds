// <auto-generated />
namespace SeedsStore.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.1-30610")]
    public sealed partial class AddTabelUserProfile : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddTabelUserProfile));
        
        string IMigrationMetadata.Id
        {
            get { return "201411052016421_AddTabelUserProfile"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
