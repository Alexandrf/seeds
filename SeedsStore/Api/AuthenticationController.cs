﻿using Newtonsoft.Json.Linq;
using SeedsStore.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using SeedsStore.Tools;
using WebMatrix.WebData;

namespace SeedsStore.Api
{
    [AllowAnonymous]
    public class AuthenticationController : ApiController
    {
        [HttpPost]
        [Route("registration")]
        public HttpResponseMessage RegistrationUser(JObject data)
        {
            var resp = new HttpResponseMessage();

            try
            {
                var errors = new Dictionary<string, string>();

                var firstName = data["FirstName"].Value<string>();
                var lastName = data["LastName"].Value<string>();
                var password = data["Password"].Value<string>();
                var email = data["Email"].Value<string>();
                var phone = data["Phone"].Value<string>();

                string confirmationToken = WebSecurity.CreateUserAndAccount(email, password, new
                {
                    FirstName = firstName,
                    LastName = lastName,
                    PhoneNumber = phone                    
                });
            
                resp.StatusCode = HttpStatusCode.Created;
                resp.Content = new StringContent(TabUser.GetJSonUser(email), Encoding.UTF8, "application/json");
                return resp;
            }
            catch (HttpException exception)
            {
                return HandlerException.HandlerHttpException(exception);
            }
            catch (Exception exception)
            {
                return HandlerException.HandlerAllException(exception);
            }
        }

        [HttpPost]
        [Route("login")]
        public HttpResponseMessage LoginUser(JObject data)
        {
            var resp = new HttpResponseMessage();

            try
            {
                var email = data["Email"].Value<string>();//Validation.GetFieldValue(data, errors, "Email", "Email");
                var password = data["Password"].Value<string>();//Validation.GetFieldValue(data, errors, "Password", "Password");
                var rememberMe = data["RememberMe"].Value<Boolean>();

                WebSecurity.Logout();

                bool success = WebSecurity.Login(email, password, rememberMe);


                if (success)
                {
                    resp = new HttpResponseMessage(HttpStatusCode.OK);
                    return resp;
                }
                else
                {
                    throw new HttpException(401, "User Unauthorised");
                }
            }
            catch (HttpException exception)
            {
                return HandlerException.HandlerHttpException(exception);
            }
            catch (Exception exception)
            {
                return HandlerException.HandlerAllException(exception);
            }
        }
    }
}