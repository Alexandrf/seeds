﻿using Newtonsoft.Json.Linq;
using SeedsStore.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using WebMatrix.WebData;

namespace SeedsStore.Controllers
{
    public class ValuesController : ApiController
    {
        [HttpPost]
        public HttpResponseMessage RegistrationUser(JObject data)
        {
            var resp = new HttpResponseMessage();

            try
            {
                // var registration = new JavaScriptSerializer().Deserialize<RegisterModel>(data.ToString());

                var errors = new Dictionary<string, string>();

                var firstName = data["FirstName"].Value<string>();//Validation.GetFieldValue(data, errors, "FirstName", "First name");
                var lastName = data["LastName"].Value<string>();//Validation.GetFieldValue(data, errors, "LastName", "Last name");
                var password = data["Password"].Value<string>();//Validation.GetFieldValue(data, errors, "Password", "Password");
                var email = data["Email"].Value<string>();//Validation.GetFieldValue(data, errors, "Email", "Email");
                var phone = data["Phone"].Value<string>();

               // Validation.CheckFieldErrors(errors);

                if (!WebSecurity.IsConfirmed(email) && WebSecurity.UserExists(email))
                {
                    throw new HttpException(403, "Forbidden");
                }

                string confirmationToken = WebSecurity.CreateUserAndAccount(email, password, new
                {
                    FirstName = firstName,
                    LastName = lastName,
                    PhoneNumber = phone
                    //BillingAccountId = CustomerController.CreatCustomer(firstName, lastName, email)//billingAccountId
                });
                

              //  var linkConfirmation = GenerateLink(confirmationToken);

                //SendEmail.SendConfirmationToken(confirmationToken, email, linkConfirmation);

               // return AddRoleByUser(roleName, email);

                resp.StatusCode = HttpStatusCode.Created;
                resp.Content = new StringContent(TabUser.GetJSonUser(email), Encoding.UTF8, "application/json");
                return resp;
            }
            catch (HttpException exception)
            {
                resp.StatusCode = HttpStatusCode.BadRequest;

                return resp;
               // return HandlerException.HandlerHttpException(exception);
            }
            catch (Exception exception)
            {
                resp.StatusCode = HttpStatusCode.BadRequest;

                return resp;
               // return HandlerException.HandlerAllException(exception);
            }
        }
    }
}
