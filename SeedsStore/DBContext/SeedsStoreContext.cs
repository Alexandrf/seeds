﻿using SeedsStore.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SeedsStore.DBContext
{
    public class SeedsStoreContext: DbContext
    {
        public SeedsStoreContext(): base("SeedsStoreContext")
        {
        }

        public DbSet<UserProfile> UserProfiles { get; set; }
    }
}