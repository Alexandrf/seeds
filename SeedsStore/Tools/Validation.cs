﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace SeedsStore.Tools
{
    public class Validation
    {
        public static string GenerateError(string error)
        {
            dynamic response = new JObject();
            var arr = new JArray();

            var descProp = new JProperty("description", error);
            var obj = new JObject(descProp);
            arr.Add(obj);
            response.errors = arr;

            var result = JsonConvert.SerializeObject(response);
            return result;
        }
    }
}