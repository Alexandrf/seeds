﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Helpers;

namespace SeedsStore.Tools
{
    public class HandlerException
    {
        public static HttpResponseMessage HandlerHttpException(HttpException exception)
        {
            var resp = new HttpResponseMessage();

            var message = exception.Message;
            var statusCode = (HttpStatusCode)exception.GetHttpCode();

            var result = Validation.GenerateError(message);

            resp = new HttpResponseMessage(statusCode);
            resp.Content = new StringContent(result, Encoding.UTF8, "application/json");
            return resp;
        }

        public static HttpResponseMessage HandlerAllException(Exception exception)
        {
            string result;
            var resp = new HttpResponseMessage();

            var message = exception.Message;

            result = Validation.GenerateError(message);

            resp = new HttpResponseMessage(HttpStatusCode.BadRequest);
            resp.Content = new StringContent(result, Encoding.UTF8, "application/json");
            return resp;
        }
    }
}